(function (cvc_services) {
  // private variables
  var district_master = [];
  var state_master = undefined;
  var languages = undefined;

  $.ajaxSetup({
    beforeSend: function(xhr) {
      if(cvc_data['auth_header']){
        xhr.setRequestHeader('Authorization', cvc_data['auth_header']);
      }
    }
  });

  function getLocationByPincode(pincode, callback){
    $.ajax({
      url : api_url+'pincode/'+ pincode,
      type: 'GET',
      success:function(data){
        callback(null,data);
      },
      error: function(err){
        callback(err, null);
      }
    });
  }

  function getStates(callback){
    if(state_master){
      return callback(null, state_master);
    }

    $.ajax({
      url : api_url+'state',
      type: 'GET',
      success:function(data){
        state_master = data;
        callback(null, data);
      },
      error: function(err){
        callback(err, null);
      }
    });
  }

  function getAnalyticsByGender(callback, is_organization){
    var url = api_url + 'analytics/individual/gender';
    if(is_organization){
      url = api_url + 'analytics/organization/gender';
    }

    $.ajax({
      url : url,
      type: 'GET',
      success:function(data){
        callback(null, data);
      },
      error: function(err){
        callback(err, null);
      }
    });
  }

  function getAnalyticsByState(callback, is_organization){
    var url = api_url + 'analytics/individual/state';
    if(is_organization){
      url = api_url + 'analytics/organization/state';
    }

    $.ajax({
      url : url,
      type: 'GET',
      success:function(data){
        callback(null, data);
      },
      error: function(err){
        callback(err, null);
      }
    });
  }

  function getAnalyticsByOrg(callback){
    var url = api_url + 'stats/cvo';

    $.ajax({
      url : url,
      type: 'GET',
      success:function(data){
        callback(null, data);
      },
      error: function(err){
        callback(err, null);
      }
    });
  }

  function getAnalyticsByAge(callback, is_organization){
    var url = api_url_local + 'analytics/individual/dob';
    if(is_organization){
      url = api_url_local + 'analytics/organization/dob';
    }

    $.ajax({
      url : url,
      type: 'GET',
      success:function(data){
        callback(null, data);
      },
      error: function(err){
        callback(err, null);
      }
    });
  }
  

  function getLanguages(callback, orgn){
    if(!orgn){
      orgn = false;
    }
    var url = api_url + 'language';
    if(orgn){
      url = api_url + 'organization/language';
    }

    $.ajax({
      url : url,
      type: 'GET',
      success:function(data){
        languages = data;
        callback(null, data);
      },
      error: function(err){
        callback(err, null);
      }
    });
  }

  function getPledgeText(lang, callback, orgn){
    if(!orgn){
      orgn = false;
    }
    var url = api_url_local+'pledge-text/'+ lang;
    if(orgn){
      url = api_url_local+'organization/pledge-text/'+ lang;
    }

    $.ajax({
      url : url,
      type: 'GET',
      success:function(data){
        callback(null, data);
      },
      error: function(err){
        callback(err, null);
      }
    });
  }

  function getPledgeStats(type, callback){
    $.ajax({
      url : api_url_local+'stats/'+ type,
      type: 'GET',
      success:function(data){
        callback(null, data);
      },
      error: function(err){
        callback(err, null);
      }
    });
  }

  function getDistrictsByState(state, callback){
    if(district_master[state]){
      return callback(null, district_master[state])
    }
    $.ajax({
      url : api_url+'district/'+ state,
      type: 'GET',
      success:function(data){
        district_master[state] = data;
        callback(null, data);
      },
      error: function(err){
        callback(err, null);
      }
    });
  };

  function createPledge(pledge_data, cb){
    $.ajax({
      url : api_url_local+'pledge',
      type: 'POST',
      contentType : 'application/json; charset=utf-8',
      // dataType:"json",
      data : pledge_data,
      success:function(data){
        cb(null, data);
      },
      error: function(err){
        cb(err, null);
      }
    });
  };

  function updatePledge(pledge_data, identity, cb){
    $.ajax({
      url : api_url_local+'pledge/' +identity,
      type: 'PUT',
      contentType : 'application/json; charset=utf-8',
      // dataType:"json",
      data : pledge_data,
      success:function(data){
        cb(null, data);
      },
      error: function(err){
        cb(err, null);
      }
    });
  };

  function createBulkPledge(pledge_data, cb){
    $.ajax({
      url : api_url_local+'bulk/pledge',
      type: 'POST',
      contentType : 'application/json; charset=utf-8',
      // dataType:"json",
      data : pledge_data,
      success:function(data){
        cb(null, data);
      },
      error: function(err){
        cb(err, null);
      }
    });
  };

  function getCertificateData(pledge_data, cb){

  
    $.ajax({
      url : api_url_local+'pledge/'+pledge_data['type']+'/'+pledge_data['identity'],
      type: 'GET',
      contentType : 'application/json; charset=utf-8',
      // dataType:"json",
      success:function(data){

        cb(null, data);
      },
      error: function(err){

        cb(err, null);
      }
    });
  };

  function getCertificate(pledge_data, cb){

    var d = {
      identity: pledge_data['identity'],
      type: pledge_data['type']
    };
    if(pledge_data['email_certificate']){
      d['email_certificate'] = pledge_data['email_certificate'];
    }
    if(pledge_data['sms_certificate']){
      d['sms_certificate'] = pledge_data['sms_certificate'];
    }

    if(pledge_data['type']){
      d['type'] = pledge_data['type'];
    }
    
    d = JSON.stringify(d);
    $.ajax({
      url : api_url_local+'certificate',
      type: 'POST',
      contentType : 'application/json; charset=utf-8',
      // dataType:"json",
      data : d,
      success:function(data){

        cb(null, data);
      },
      error: function(err){

        cb(err, null);
      }
    });
  };

  function getCertificateBulk(pledge_data, cb){

    var d = {
      identity: pledge_data['identity'],
      reference_number: pledge_data['reference_number']
    };
    d['type'] = 'mass';

    if(pledge_data['email_certificate']){
      d['email_certificate'] = pledge_data['email_certificate'];
    }

    d = JSON.stringify(d);
    $.ajax({
      url : api_url_local+'bulk/certificate',
      type: 'POST',
      contentType : 'application/json; charset=utf-8',
      // dataType:"json",
      data : d,
      success:function(data){

        cb(null, data);
      },
      error: function(err){

        cb(err, null);
      }
    });
  };

  function getAllUrlParams(url) {
    // get query string from url (optional) or window
    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

    // we'll store the parameters here
    var obj = {};

    // if query string exists
    if (queryString) {

      // stuff after # is not part of query string, so get rid of it
      queryString = queryString.split('#')[0];

      // split our query string into its component parts
      var arr = queryString.split('&');

      for (var i=0; i<arr.length; i++) {
        // separate the keys and the values
        var a = arr[i].split('=');

        // in case params look like: list[]=thing1&list[]=thing2
        var paramNum = undefined;
        var paramName = a[0].replace(/\[\d*\]/, function(v) {
          paramNum = v.slice(1,-1);
          return '';
        });

        // set parameter value (use 'true' if empty)
        var paramValue = typeof(a[1])==='undefined' ? true : a[1];

        // (optional) keep case consistent
        paramName = paramName.toLowerCase();
        paramValue = paramValue.toLowerCase();

        // if parameter name already exists
        if (obj[paramName]) {
          // convert value to array (if still string)
          if (typeof obj[paramName] === 'string') {
            obj[paramName] = [obj[paramName]];
          }
          // if no array index number specified...
          if (typeof paramNum === 'undefined') {
            // put the value on the end of the array
            obj[paramName].push(paramValue);
          }
          // if array index number specified...
          else {
            // put the value at that index number
            obj[paramName][paramNum] = paramValue;
          }
        }
        // if param name doesn't exist yet, set it
        else {
          obj[paramName] = paramValue;
        }
      }
    }

    return obj;
  }

  cvc_services['getLocationByPincode'] = getLocationByPincode;
  cvc_services['createPledge'] = createPledge;
  cvc_services['updatePledge'] = updatePledge;  
  cvc_services['getStates'] = getStates;
  cvc_services['getDistrictsByState'] =getDistrictsByState;
  cvc_services['getLanguages'] = getLanguages;
  cvc_services['getPledgeText'] = getPledgeText;
  cvc_services['getCertificate'] = getCertificate;
  cvc_services['getCertificateData'] = getCertificateData;
  cvc_services['getCertificateBulk'] = getCertificateBulk;
  cvc_services['getPledgeStats'] = getPledgeStats;
  cvc_services['getAllUrlParams'] = getAllUrlParams;
  cvc_services['createBulkPledge'] = createBulkPledge;
  cvc_services['createBulkPledge'] = createBulkPledge;
  cvc_services['getAnalyticsByState'] = getAnalyticsByState;
  cvc_services['getAnalyticsByGender'] = getAnalyticsByGender;
  cvc_services['getAnalyticsByAge'] = getAnalyticsByAge;
  cvc_services['getAnalyticsByOrg'] = getAnalyticsByOrg;

})(cvc_services);
