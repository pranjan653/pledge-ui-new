(function(cvc_services){
  function populateStateDropDown(selector, values, highlighed_value){
    $(selector).append('<option value="">---Select---</option>');
    values.forEach(function(val){
      var html = $('<option value="'+val+'">'+val+'</option>');
      if(highlighed_value == val){
        $(html).attr('selected', 'selected');
      }
      $(selector).append(html);
    });
  }

  function populateDistrictDropDown(selector, values, highlighed_value){
    $(selector).find('option').remove();
    $(selector).append('<option value="">---Select---</option>');        
    values.forEach(function(val){
      var html = $('<option value="'+val+'">'+val+'</option>');
      if(highlighed_value == val){
        $(html).attr('selected', 'selected');
      }
      $(selector).append(html);
    });

  }

  function showMsg(type, msg, is_debug){
    if(!is_debug){
      is_debug = false;
    }
    // 'warning', 'info', 'success', 'error'
    if(is_debug && !debug_mode){
      return;
    }

    Lobibox.notify(type, {  
      size: 'mini',
      rounded: true,
      delayIndicator: false,   
      position: 'center top',
      sound: false,      
      icon: false, 
      msg: msg
    });

  }

  cvc_services['populateStateDropDown'] = populateStateDropDown;
  cvc_services['populateDistrictDropDown'] = populateDistrictDropDown;
  cvc_services['showMsg'] = showMsg;

})(cvc_services);